import datetime

from flask import Flask, jsonify, request


def create_app():
    app = Flask(__name__)

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///prod.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    from app.app_main import db

    db.init_app(app)
    from app.modules import Client, Client_parking, Parking

    @app.before_request
    def start():
        db.create_all()

    @app.teardown_appcontext
    def the_end(exception=None):
        db.session.remove()

    @app.route("/clients", methods=["GET"])
    def get_clients():
        clients = db.session.query(Client).all()
        list_clients = []
        for cl in clients:
            client = cl.to_json()
            list_clients.append(client)

        return list_clients, 200

    @app.route("/client/<client_id>", methods=["GET"])
    def get_client_id(client_id):
        client = db.session.query(Client).filter(Client.id == client_id).first()
        client_json = client.to_json()
        return client_json, 200

    @app.route("/clients", methods=["POST"])
    def add_client():
        name = request.form.get("name", type=str)
        surname = request.form.get("surname", type=str)
        credit_card = request.form.get("credit_card", type=str)
        car_number = request.form.get("car_number", type=str)

        new_client = Client(
            name=name, surname=surname, credit_card=credit_card, car_number=car_number
        )

        db.session.add(new_client)
        db.session.commit()
        return "Add new client", 201

    @app.route("/parking", methods=["POST"])
    def add_place_parking():
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=bool)
        count_placed = request.form.get("count_placed", type=int)
        count_available_places = request.form.get("count_available_places", type=int)

        new_parking = Parking(
            address=address,
            opened=opened,
            count_placed=count_placed,
            count_available_places=count_available_places,
        )

        db.session.add(new_parking)
        db.session.commit()
        return "Add new place parking", 201

    @app.route(
        "/clients_parking/<client_id>/<parking_id>", methods=["POST"], endpoint="add"
    )
    def delete_place_parking(client_id, parking_id):
        parking = (
            db.session.query(Parking.opened).filter(Parking.id == parking_id).first()
        )
        # parking = db.session.query(Parking.id, Parking.opened).filter(Parking.address == address).first()
        # client = db.session.query(Client.id).filter(Client.car_number == car_number).first()
        client_parking = (
            db.session.query(Client_parking.id)
            .filter(
                db.and_(Client_parking.client_id == client_id),
                (Client_parking.parking_id == parking_id),
            )
            .one_or_none()
        )
        if client_parking:
            time = datetime.datetime.now()
            time_out = datetime.datetime(2023, 12, 24, 22, 54, 25, 5242)
            print(time)
            (
                db.session.query(Client_parking)
                .filter(Client_parking.client_id == client_id)
                .update({"time_in": time, "time_out": time_out})
            )
            db.session.commit()
        else:
            if parking[0]:
                db.session.query(Parking).update(
                    {"count_available_places": Parking.count_available_places - 1}
                )
                time_in = datetime.datetime.now()
                new_client_parking = Client_parking(
                    time_in=time_in, client_id=client_id, parking_id=parking_id
                )
                db.session.add(new_client_parking)
                db.session.commit()

        available_place_parking = db.session.query(
            Parking.count_available_places
        ).first()
        if available_place_parking[0] == 0:
            db.session.query(Parking).update({"opened": False})
        resp = (
            db.session.query(Client_parking)
            .filter(Client_parking.id == client_parking[0])
            .first()
        )
        return jsonify(resp=resp.to_json()), 201

    @app.route(
        "/clients_parking/<client_id>/<parking_id>",
        methods=["DELETE"],
        endpoint="delete",
    )
    def delete_place_parking(client_id, parking_id):
        time_out = datetime.datetime.now()
        # client = db.session.query(Client.id).filter(Client.car_number == car_number).first()
        db.session.query(Client_parking).filter(
            Client_parking.client_id == client_id
        ).update({"time_out": time_out})
        db.session.query(Parking).update(
            {"count_available_places": Parking.count_available_places + 1}
        )
        db.session.commit()
        resp = (
            db.session.query(Client_parking)
            .filter(Client_parking.client_id == client_id)
            .first()
        )
        return str(resp), 201

    @app.route("/", methods=["GET"])
    def test_page():
        return "OK"

    return app
