from typing import Any, Dict

from sqlalchemy import Integer, DateTime, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship, mapped_column, Mapped

from app.app_main import db


class Client(db.Model):
    __tablename__ = "clients"

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True, nullable=False)
    name: Mapped[String] = mapped_column(String(50), nullable=False)
    surname: Mapped[String] = mapped_column(String(50))
    credit_card: Mapped[Integer] = mapped_column(String(50))
    car_number: Mapped[String] = mapped_column(String(10))

    def __repr__(self):
        return f"Клиент {self.name} {self.surname}"

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Parking(db.Model):
    __tablename__ = "parking_count"

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True, nullable=False)
    address: Mapped[String] = mapped_column(String(100), nullable=False)
    opened: Mapped[Boolean] = mapped_column(Boolean)
    count_placed: Mapped[Integer] = mapped_column(Integer, nullable=False)
    count_available_places: Mapped[Integer] = mapped_column(Integer, nullable=False)

    def __repr__(self):
        return f"Адрес парковки {self.address}"

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Client_parking(db.Model):
    __tablename__ = "clients_parking"

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True, nullable=False)
    time_in: Mapped[DateTime] = mapped_column(DateTime)
    time_out: Mapped[DateTime] = mapped_column(DateTime)
    client_id: Mapped[Integer] = mapped_column(
        Integer, ForeignKey("clients.id"), unique=True
    )
    client: Mapped["Client"] = relationship("Client", backref="clients_parking")
    parking_id: Mapped[Integer] = mapped_column(
        Integer, ForeignKey("parking_count.id"), unique=True
    )
    parking: Mapped["Parking"] = relationship("Parking", backref="clients_parking")

    def __repr__(self):
        time = self.time_out - self.time_in
        return f"Время стоянки {time}"

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
