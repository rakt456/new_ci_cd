import datetime

import pytest as pytest

from app.app_route import create_app
from app.app_main import db as _db
from app.modules import Client, Client_parking, Parking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()

        client = Client(
            id=1,
            name="Vova",
            surname="Pupkin",
            credit_card=1425963578535742,
            car_number="M735DV",
        )

        parking = Parking(
            id=1,
            address="Moscow",
            opened=True,
            count_placed=50,
            count_available_places=3,
        )

        client_parking = Client_parking(
            time_in=datetime.datetime(2023, 12, 20, 22, 54, 25, 5242),
            time_out=datetime.datetime(2023, 12, 22, 8, 5, 25, 4275),
            client_id=1,
            parking_id=1,
        )
        _db.session.add(client)
        _db.session.add(parking)
        _db.session.add(client_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
