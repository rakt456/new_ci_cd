import random

import factory
from faker import Faker
from faker_vehicle import VehicleProvider

from app.modules import Client, Parking
from app.app_main import db

fake = Faker()
fake.add_provider(VehicleProvider)


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name: Faker = factory.Faker("first_name")
    surname: Faker = factory.Faker("last_name")
    credit_card: Faker = factory.Faker("pybool")
    car_number: Faker = fake.machine_year_make_model()


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address: Faker = factory.Faker("address")
    opened: Faker = factory.Faker("pybool")
    count_placed: Faker = factory.LazyAttribute(lambda x: random.randrange(2, 30))
    count_available_places: Faker = factory.LazyAttribute(
        lambda x: random.randint(1, x.count_placed)
    )
