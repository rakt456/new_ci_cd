import pytest


@pytest.mark.parametrize("route", ["/clients", "/client/1"])
def test_status_code(client, route):
    resp = client.get(route)
    assert resp.status_code == 200


def test_client(client):
    client_data = {
        "name": "Hola",
        "surname": "Govok",
        "credit_card": 1542365478529654,
        "car_number": "B741RV",
    }
    resp = client.post("/clients", data=client_data)
    assert resp.status_code == 201


def test_parking(client):
    parking_data = {
        "address": "London",
        "opened": True,
        "count_placed": 15,
        "count_available_places": 2,
    }
    resp = client.post("/parking", data=parking_data)
    assert resp.status_code == 201


@pytest.mark.parking
def test_enter_parking(client):
    resp = client.post("/clients_parking/1/1")
    assert resp.status_code == 201


@pytest.mark.parking
def test_exit_parking(client):
    resp = client.delete("/clients_parking/1/1")
    assert resp.status_code == 201
