from app.modules import Client, Parking
from app.tests.factories import ClientFactory, ParkingFactory


def test_create_client(app, db):
    client_t = ClientFactory()
    db.session.commit()
    assert client_t.id is not None
    assert len(db.session.query(Client).all()) == 2


def test_create_parking(client, db):
    parking = ParkingFactory()
    db.session.commit()
    assert parking.id is not None
    assert len(db.session.query(Parking).all()) == 2
